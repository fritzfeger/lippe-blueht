# Reporting Vollgas und Lippe blüht

Auf dieser Webseite werden Daten aus der internen Betriebsbuchhaltung mit Podio angezeigt:

* https://fritzfeger.gitlab.io/lippe-blueht/

Das statische HTML ist generiert mit [Lektor](https://www.getlektor.com/); die dynamischen JS-Inserts aus [Podio](https://www.podio.com/) werden mit [GlobiFlow](https://www.globiflow.com/) erzeugt.

Im Moment ist die Seite öffentlich, d.h. jeder, der den Link hat, kann die Daten sehen. So lange wir den Link nicht weitergeben, findet das i.d.R. niemand Unbefugtes, aber SICHER ist das überhaupt nicht.

Was auch möglich wäre: Die Seite ist nur für "Members" sichtbar, d.h. man braucht einen GitLab-Account und muß vom Maintainer zum Projekt hinzugefügt worden sein.

Dokumentation in Notion:
* https://www.notion.so/Einheitlicher-Kontenplan-Buchstelle-und-AP-674687445f6f4cb8a2a236e8a94acc29
